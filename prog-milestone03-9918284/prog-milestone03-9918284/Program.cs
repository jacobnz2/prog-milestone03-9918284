﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//What a headache! OMG!

namespace milestone3
{
    class Program
    {
        static Customer Customer;
        static Dictionary<string, string> PizzaToppings;
        static Dictionary<string, string> PizzaSizes;
        static Dictionary<string, float> PizzaPrices;
        static Dictionary<string, string> BeerChoice;

        const float BeerPrice = 2.5f;

        static void Main(string[] args)
        {
            PizzaToppings = new Dictionary<string, string>();
            PizzaToppings.Add("1", "Mushroom");
            PizzaToppings.Add("2", "Apricot Chicken");
            PizzaToppings.Add("3", "Jalepeno");
            PizzaToppings.Add("4", "Cheese");
            PizzaToppings.Add("5", "Meat Lovers");
            PizzaToppings.Add("6", "Fruit");

            PizzaSizes = new Dictionary<string, string>();
            PizzaSizes.Add("1", "Small");
            PizzaSizes.Add("2", "Regular");
            PizzaSizes.Add("3", "Large");

            PizzaPrices = new Dictionary<string, float>();
            PizzaPrices.Add("1", 3);
            PizzaPrices.Add("2", 6);
            PizzaPrices.Add("3", 9);

            BeerChoice = new Dictionary<string, string>();
            BeerChoice.Add("1", "Corona");
            BeerChoice.Add("2", "Heineken");
            BeerChoice.Add("3", "Steignlager");
            BeerChoice.Add("4", "Peroni");
            BeerChoice.Add("5", "Stella Artois");
            BeerChoice.Add("6", "Great White");


            Menu();
        }

        static void Order()
        {
            //If they choose to order it is now necessary to ask their details
            Customer = new Customer();
            Customer.OrderDetail = new Order();
            Customer.OrderDetail.Pizzas = new List<Pizza>();
            Customer.OrderDetail.Beers = new List<Beer>();

            Console.Clear();
            Console.WriteLine("May I have your name Please? ");
            Customer.Name = Console.ReadLine();

            Console.Clear();
            Console.WriteLine($"Okay, thank you {Customer.Name}. May I please have your contact number? ");
            Customer.PhoneNumber = Console.ReadLine();

            OrderPizza();

        }

        static void Menu()
        {
            //Ask customer if they would like to place an order or not.
            Console.Clear();
            Console.WriteLine("    Poorboys Pizza & Beer   ");
            Console.WriteLine();
            Console.WriteLine("****************************");
            Console.WriteLine("*                          *");
            Console.WriteLine("*    1. Place an Order     *");
            Console.WriteLine("* 2. Leave Poorboys Pizza  *");
            Console.WriteLine("*          & Beer          *");
            Console.WriteLine("*                          *");
            Console.WriteLine("****************************");

            Console.WriteLine();
            Console.WriteLine();
            Console.Write("What would you like to do? ");
            var option = Console.ReadLine();

            if (option == "1")
                Order();
            else if (option == "2")
                return;
            else Menu();

        }



        static void OrderPizza()
        {
            //menu screen to select toppings
            Console.Clear();
            Console.WriteLine();
            Console.WriteLine($"Okay {Customer.Name} Please select your toppings.");
            Console.WriteLine();
            Console.WriteLine("**************************************");
            Console.WriteLine();
            foreach (var pizza in PizzaToppings)
            {
                Console.WriteLine(pizza.Key + ". " + pizza.Value);
            }
            Console.WriteLine("");
            Console.WriteLine("0. Back to Main Menu");
            Console.WriteLine();
            Console.WriteLine("**************************************");
            Console.WriteLine();
            Console.Write("Select your Pizza and press <ENTER>");

            //selecting pizzas for final order
            var choice = Console.ReadLine();
            if (PizzaToppings.ContainsKey(choice))
            {
                Pizza pizza = new Pizza();
                pizza.Style = PizzaToppings[choice];
                OrderPizzaSize(pizza);
            }
            else if (choice == "0")
                Menu();
            else OrderPizza();
        }

        static void OrderPizzaSize(Pizza pizza)
        {
            //selecting sizes of selected pizzas
            Console.Clear();
            Console.WriteLine($"What size would you like your {pizza.Style} pizza?");
            Console.WriteLine();
            Console.WriteLine("*********************************************");
            Console.WriteLine();
            foreach (var size in PizzaSizes)
            {
                Console.WriteLine(size.Key + ". " + size.Value);
            }
            Console.WriteLine();
            Console.WriteLine("*********************************************");
            Console.WriteLine();
            Console.Write("Enter Selection: ");
            var option = Console.ReadLine();
            if (PizzaSizes.ContainsKey(option))
            {
                pizza.Size = PizzaSizes[option];
                pizza.Cost = PizzaPrices[option];
                Customer.OrderDetail.Pizzas.Add(pizza);
                AddPizza();
            }
            else OrderPizzaSize(pizza);
        }

        static void AddPizza()
        {
            //Decides how many pizzas to order before moving on
            Console.Clear();
            Console.Write("Would you like to add another pizza to your order? (y/n): ");
            var option = Console.ReadLine();
            if (option == "y")
                OrderPizza();
            else if (option == "n")
                OrderBeers();
            else AddPizza();
        }

        static void OrderBeers()
        {
            //Listing the selected beers with prices
            Console.Clear();
            if (Customer.OrderDetail.Beers.Count > 0)
            {
                Console.WriteLine("Beers you've selected so far...");
                Console.WriteLine();
                Console.WriteLine("-------------------------------");
                foreach (var drink in Customer.OrderDetail.Beers)
                {
                    Console.WriteLine("- Beer: " + drink.Name + " $" + drink.Cost);
                }
                Console.WriteLine("-------------------------------");
                Console.WriteLine();
            }

            //Selecting beers for final order
            Console.WriteLine("*************************************************");
            Console.WriteLine();
            Console.WriteLine("Would you like a bitter beverage with your order?");
            Console.WriteLine();
            Console.WriteLine("*************************************************");
            Console.WriteLine();
            foreach (var beer in BeerChoice)
            {
                Console.WriteLine(beer.Key + ". " + beer.Value);
            }
            //Order Completion
            Console.WriteLine("");
            Console.WriteLine("0. Proceed with order");
            Console.WriteLine();
            Console.WriteLine("*************************************************");
            Console.WriteLine();
            var option = Console.ReadLine();
            if (BeerChoice.ContainsKey(option))
            {
                Beer beer = new Beer();
                beer.Name = BeerChoice[option];
                beer.Cost = BeerPrice;
                Customer.OrderDetail.Beers.Add(beer);

                OrderBeers();
            }
            else if (option == "0")
                PayPoorboy();
            else OrderBeers();

        }

        static void PayPoorboy()
        {
            //Reading back the customers details
            Console.Clear();
            Console.WriteLine("Your order is as follows...");
            Console.WriteLine();
            Console.WriteLine("***************************");
            Console.WriteLine();
            Console.WriteLine("Name: " + Customer.Name);
            Console.WriteLine("Phone: " + Customer.PhoneNumber);
            Console.WriteLine();
            Console.WriteLine("***************************");
            Console.WriteLine();

            float totalcost = 0;
            foreach (var pizza in Customer.OrderDetail.Pizzas)
            {
                //Calculating pizza costs
                Console.WriteLine("Pizza: " + pizza.Size + " " + pizza.Style + " $" + pizza.Cost);
                totalcost += pizza.Cost;
            }

            if (Customer.OrderDetail.Beers.Count > 0)
            {
                //Calculating beer costs
                Console.WriteLine();
                Console.WriteLine("***************************************");
                Console.WriteLine();
                foreach (var beer in Customer.OrderDetail.Beers)
                {
                    Console.WriteLine("Beer: " + beer.Name + " $" + beer.Cost);
                    totalcost += beer.Cost;
                }
            }
            //Calculating the total cost
            Console.WriteLine();
            Console.WriteLine("***************************************");
            Console.WriteLine();
            Console.WriteLine("That comes to a total of: $" + totalcost);
            Console.WriteLine();
            Console.WriteLine("***************************************");
            Console.WriteLine();
            Console.WriteLine("");
            Console.Write("Select y to proceed to checkout or hit another key to go back to the main menu: ");

            var option = Console.ReadLine();
            if (option == "y")

            //Customer chooses if they wish to check out or go back to menu
            {
                Console.Clear();
                Console.WriteLine("Thank you for choosing Poorboy's Pizza & Beer!  Your order should be ready soon");
                Console.ReadKey();
                Console.Clear();
                Console.WriteLine("Have a good day");
                Console.WriteLine();
            }


        }

    }

    //All my classes
    public class Customer
    {
        public string Name;
        public string PhoneNumber;
        public Order OrderDetail;
    }

    public class Order
    {
        public List<Pizza> Pizzas;
        public List<Beer> Beers;
    }

    public class Pizza
    {
        public string Style;
        public string Size;
        public float Cost;
    }

    public class Beer
    {
        public string Name;
        public float Cost;
    }
}